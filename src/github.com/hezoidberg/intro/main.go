package main

import (
	"fmt"
	"math"
	"math/big"
	"os"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

var (
	wg            sync.WaitGroup
	numberCounter int64 = 0
)

func getLetter(number int64) string {
	if number > 9 {
		return string(number + 87)
	} else {
		return fmt.Sprintf("%d", number)
	}
}

func getNumber(str rune) int64 {
	code := int64(str)
	if code > 96 {
		return code - 87
	} else {
		return code - 48
	}
}

func convertFrom10(number int64, base int64) string {
	str := ""
	for number >= base {
		mod := number % base
		str = getLetter(mod) + str
		number /= base
	}
	return getLetter(number) + str
}

func convertTo10(numberToConvert string, base int64) int64 {
	var res int64 = 0
	if base == 10 {
		number := big.NewInt(0)
		number.SetString(numberToConvert, 10)
		return number.Int64()
	}

	n := len(numberToConvert) - 1
	for i, runeVal := range numberToConvert {
		res += getNumber(runeVal) * int64(math.Pow(float64(base), float64(n-i)))
	}

	return res
}

func convert(numberToConvert string, baseTo int64, baseFrom int64, ch chan string) {
	defer wg.Done()

	res := convertFrom10(convertTo10(numberToConvert, baseFrom), baseTo)
	ch <- res
}

func formattedPrint(count int64, total int64, ch chan string, routinesCount int) {
	for i := 1; i <= routinesCount; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()

			elements := []string{}
			for convertedNum := range ch {
				elements = append(elements, convertedNum)
				atomic.AddInt64(&numberCounter, 1)
				if len(elements) == int(count) || total == atomic.LoadInt64(&numberCounter) {
					fmt.Println(strings.Join(elements, ", "))
					elements = []string{}
				}
			}
		}(i)
	}
}

func main() {
	if len(os.Args) != 5 {
		fmt.Println("invalid count of args")
		os.Exit(2)
	}

	baseToStr := os.Args[1]
	baseFromStr := os.Args[2]
	numToConvert := os.Args[3]
	stringElementsCount := os.Args[4]

	baseToBig := big.NewInt(0) // Crutch for parsing
	baseFromBig := big.NewInt(0)
	strCountBig := big.NewInt(0)

	baseToBig.SetString(baseToStr, 10)
	baseFromBig.SetString(baseFromStr, 10)
	strCountBig.SetString(stringElementsCount, 10)
	c := strings.ToLower(numToConvert)

	baseTo := baseToBig.Int64()
	baseFrom := baseFromBig.Int64()
	strCount := strCountBig.Int64()

	ch := make(chan string, int(strCount))

	for i := int64(2); i <= baseTo; i++ {
		wg.Add(1)
		go convert(c, i, baseFrom, ch)
	}

	total := baseTo - 1
	formattedPrint(strCount, total, ch, 2)

	//	wg.Wait()
	time.Sleep(time.Second)
}
